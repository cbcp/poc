# cbcp
## Cryptocurrency backed copy protection

cbcp is a collection of smart contracts, currently targeting the Ethereum virtual machine and designed to stop the sharing of content.

This is achieved by locking collateral into the protocol before distributing the content to the user in a manner which ensures the content is encrypted with the public key of the Ethereum wallet. As such, the user needs the private key to access the content. If the user shares the content, and hence the private key for the Ethereum wallet, the receiver of the shared content can sign a transaction which entitles them to a share of the collateral, while some of the collateral is allocated to the provider of the content as compensation.

This repository contains a proof of concept implementation of the protocol.
