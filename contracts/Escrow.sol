pragma solidity 0.5.0;

import "./SafeMath.sol";
import "./AddressConv.sol";


contract Escrow {

	using SafeMath for uint;

	struct User {
		uint expiryTime;	// Time when the user can withdraw
		uint lockedAmount;	// Amount locked until expiry
		uint amountToReturn;	// The amount to pay to claimants
		uint returnedAmount;	// Amount paid to claimants
		uint paidAmount;	// Amount paid to trustee
		uint numOfClaims;	// Number of people claiming deposit
		mapping (uint => address) claimants;	// Accounts to release funds to
	}

	struct Server {
		address beneficiary;	// Account to send funds to
		address escrowAdmin;	// Account to administer the escrow
		uint paymentAmount;	// Standard payment amount
		bool initiated;		// Server has been initiated
		uint numOfClients;	// Number of clients using this server's escrow
		mapping (uint => address) clients;	//Clients of this contract
	}

	address private _owner;

	bool private _valid;

	uint public numOfServers = 0;

	mapping(uint => address) public listOfServers;

	mapping(address => uint) public withdrawalBalance;

	mapping(address => Server) public servers;

	mapping(address => mapping(address => User)) public users;

	event NewServer(
		address contractOpener,
		address beneficiary,
		uint paymentAmount
	);

	event newUser(
		address contractOpener,
		address user,
		uint expiryTime,
		uint lockedAmount
	);

	event NewClaimant(
		address contractOpener,
		address user,
		address newClaimant
	);

	event ProcessedEscrow(
		address contractOpener,
		address user,
		address releasedTo,
		uint amount
	);

	event HastenedEscrow(
		address contractOpener,
		address user,
		uint newExpiryTime
	);

	modifier beforeTimeout(
		address escrowAdmin,
		address depositer
	) {
		require(users[escrowAdmin][depositer].expiryTime > now,
			"Make sure time is before expiry");
		_;
	}

	modifier afterTimeout(
		address escrowAdmin,
		address depositer
	) {
		require(users[escrowAdmin][depositer].expiryTime < now,
			"Current time is passed the expiry");
		_;
	}

	modifier isOwner() {
		require(msg.sender == _owner,
			"Must be the creator of the contract");
		_;
	}

	modifier isValid() {
		require(_valid,
			"Contract is no longer valid");
		_;
	}

	constructor() public {
		_owner = msg.sender;
		_valid = true;
	}

	function voidContract()
		public
		isOwner
	{
		_valid = false;
	}

	function createContract(address _beneficiary)
		public
		isValid
		payable
	{
		require(!servers[msg.sender].initiated,
			"Already initiated escrow");
		servers[msg.sender] = Server({
			beneficiary: _beneficiary,
			escrowAdmin: msg.sender,
			paymentAmount: msg.value,
			initiated: true,
			numOfClients: 0
		});
		listOfServers[numOfServers++] = msg.sender;

		emit NewServer(
			msg.sender,
			_beneficiary,
			msg.value
		);

		withdrawalBalance[_beneficiary] = 
			withdrawalBalance[_beneficiary].add(msg.value);
	}

	function openNewEscrow(
		address escrowAdmin,
		uint timeout
	)
		public
		payable
	{
		require(timeout > 0, "timeout must be positive");
		require(servers[escrowAdmin].initiated, "Server must be initiated");
		require(users[escrowAdmin][msg.sender].lockedAmount == 0,
			"Users can only create a single instance with each server");
		
		users[escrowAdmin][msg.sender] = User({
			expiryTime: 	now + timeout,
			lockedAmount: 	msg.value,
			amountToReturn:	0,
			returnedAmount:	0,
			paidAmount: 	0,
			numOfClaims:	0
		});

		servers[escrowAdmin].clients[servers[escrowAdmin].numOfClients++] = msg.sender;

		emit newUser(
			escrowAdmin,
			msg.sender,
			now + timeout,
			msg.value
		);
	}

	function submitRefund(
		address escrowAdmin,
		address refundTo
	)
		public
		beforeTimeout(escrowAdmin, msg.sender)
		isValid
	{
		users[escrowAdmin][msg.sender].claimants[
			users[escrowAdmin][msg.sender].numOfClaims++
		] = refundTo;

		emit NewClaimant(
			escrowAdmin,
			msg.sender,
			refundTo
		);
	}

	function submitSignedRefund(
		bytes32 h,
		uint8 v,
		bytes32 r,
		bytes32 s,
		address escrowAdmin,
		address depositer,
		address refundTo
	)
		public
		beforeTimeout(escrowAdmin, depositer)
		isValid
	{
		require(checkSig(h, v, r, s, depositer, abi.encodePacked(
			this,
			escrowAdmin,
			depositer,
			refundTo
		)), "Invalid signature");

		users[escrowAdmin][depositer].claimants[
			users[escrowAdmin][depositer].numOfClaims++
		] = refundTo;

		emit NewClaimant(
			escrowAdmin,
			depositer,
			refundTo
		);
	}


	function processRefund(
		address escrowAdmin,
		address depositer,
		address refundTo
	)
		public
		afterTimeout(escrowAdmin, depositer)
	{
		uint numOfClaims = users[escrowAdmin][depositer].numOfClaims;
		if (numOfClaims == 0) {
			users[escrowAdmin][depositer].claimants[
				users[escrowAdmin][depositer].numOfClaims++
			] = depositer;

			numOfClaims = 1;
		}

		uint valueToReturn = getReturnValue(escrowAdmin, depositer, numOfClaims);
		for (uint i = 0; i < numOfClaims; i++) {
			if (users[escrowAdmin][depositer].claimants[i] == refundTo) {
				delete users[escrowAdmin][depositer].claimants[i];
				withdrawalBalance[refundTo] = 
					withdrawalBalance[refundTo].add(valueToReturn);
			}
		}

		emit ProcessedEscrow(
			escrowAdmin,
			depositer,
			refundTo,
			valueToReturn
		);
	}

	function hastenExpiry(
		bytes32 h,
		uint8 v,
		bytes32 r,
		bytes32 s,
		address escrowAdmin,
		uint hastenExpiryBy
	)
		public
	{
		require(users[escrowAdmin][msg.sender].expiryTime.sub(hastenExpiryBy) > now,
			"Cannot hasten expiry further than current time");

		require(checkSig(h, v, r, s, escrowAdmin, abi.encodePacked(
			this,
			escrowAdmin,
			msg.sender,
			hastenExpiryBy
		)), "Invalid signature");

		users[escrowAdmin][msg.sender].expiryTime =
			users[escrowAdmin][msg.sender].expiryTime.sub(hastenExpiryBy);

		emit HastenedEscrow(
			escrowAdmin,
			msg.sender,
			users[escrowAdmin][msg.sender].expiryTime
		);
	}

	using address_make_payable for address;

	function withdraw(address toAddr)
		public
	{
		address payable addr = toAddr.make_payable();
		uint amount = withdrawalBalance[addr];
		if (amount > 0) {
			withdrawalBalance[addr] = 0;
			require(addr.send(amount));
		}
	}

	function checkSig(
		bytes32 h,
		uint8 v,
		bytes32 r,
		bytes32 s,
		address signedBy,
		bytes memory data
	)
		internal
		pure
		returns (bool valid)
	{
		address signer;
		bytes32 proof;

		bytes memory prefix = "\x19Ethereum Signed Message:\n32";
		bytes32 prefixedHash = keccak256(abi.encodePacked(prefix, h));
		signer = ecrecover(prefixedHash, v, r, s);

		require(signer == signedBy, "Signature is invalid");

		proof = keccak256(data);

		require(proof == h, "Signature mismatch with data");

		return true;
	}

	function getAmountToPay(
		uint lockedAmount,
		uint paymentAmount,
		uint numOfClaims
	)
		internal
		pure
		returns (uint returnValue)
	{
		returnValue = (lockedAmount.sub(
			paymentAmount.mul(numOfClaims))
			).div(2 ** (numOfClaims.sub(1)));
	}
	
	function getReturnValue(
		address escrowAdmin,
		address depositer,
		uint numOfClaims
	)
		internal
		returns (uint value)
	{
		if (users[escrowAdmin][depositer].amountToReturn == 0) {
			if (servers[escrowAdmin].paymentAmount.mul(numOfClaims) >
				users[escrowAdmin][depositer].lockedAmount)
			{
				users[escrowAdmin][depositer].amountToReturn = 0;
			} else {
				users[escrowAdmin][depositer].amountToReturn = getAmountToPay(
					users[escrowAdmin][depositer].lockedAmount,
					servers[escrowAdmin].paymentAmount,
					numOfClaims
				);
			}
		}
		if (users[escrowAdmin][depositer].paidAmount == 0) {
			uint amountToPay = users[escrowAdmin][depositer].lockedAmount
			.sub(users[escrowAdmin][depositer].amountToReturn.mul(numOfClaims));
			if (amountToPay > 0) {
				users[escrowAdmin][depositer].paidAmount = amountToPay;
				withdrawalBalance[servers[escrowAdmin].beneficiary] = 
					withdrawalBalance[servers[escrowAdmin].beneficiary].add(amountToPay);
			}
		}
		return users[escrowAdmin][depositer].amountToReturn;
	}
}


