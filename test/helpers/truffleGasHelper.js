getUsedGas = async res => {
	tx = await web3.eth.getTransaction(res.tx);
	gas = await res.receipt.gasUsed;
	return Number(tx.gasPrice) * gas;
}

module.exports = {
	getUsedGas
}
