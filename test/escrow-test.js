const BN = web3.utils.BN;
const Escrow = artifacts.require("./Escrow");
const timeHelper = require("./helpers/truffleTimeHelper");
const gasHelper = require('./helpers/truffleGasHelper');

contract('Escrow', async accounts => {
	let meta;

	let amount = web3.utils.toWei('0.1', 'ether');
	let contractOpener = accounts[2];
	let beneficiary = accounts [9];
	let depositAmount = web3.utils.toWei('0.5', 'ether');
	let timeout = 10;


	it('Make sure no functions, except opening a new escrow, can run with a new contract', async () => {
		meta = await Escrow.new();
		
		try {
			let res1 = await meta.openNewEscrow(contractOpener, timeout, {from: accounts[3], value: amount});
			console.log(res1);
			assert(false, 'Expected revert but did not receive this');
		} catch (error) {
			const revertFound = error.message.search('revert') >= 0;
			assert(revertFound, 'Expected revert, got ${error} instead');
		}

		try {
			let res1 = await meta.submitRefund(contractOpener, accounts[3], {from: accounts[3]});
			console.log(res1);
			assert(false, 'Expected revert but did not receive this');
		} catch (error) {
			const revertFound = error.message.search('revert') >= 0;
			assert(revertFound, 'Expected revert, got ${error} instead');
		}

		try {
			let res1 = await meta.submitSignedRefund('0x','0x','0x','0x', contractOpener, accounts[3], accounts[3], {from: accounts[3]});
			assert(false, 'Expected revert but did not receive this');
		} catch (error) {
			const revertFound = error.message.search('revert') >= 0;
			assert(revertFound, 'Expected revert, got ${error} instead');
		}

		try {
			let res1 = await meta.processRefund(contractOpener, accounts[3], accounts[3], {from: accounts[3]});
			assert(false, 'Expected revert but did not receive this');
		} catch (error) {
			const revertFound = error.message.search('revert') >= 0;
			assert(revertFound, 'Expected revert, got ${error} instead');
		}

		try {
			let res1 = await meta.hastenExpiry('0x','0x','0x','0x', contractOpener, '', {from: accounts[3]});
			assert(false, 'Expected revert but did not receive this');
		} catch (error) {
			const revertFound = error.message.search('revert') >= 0;
			assert(revertFound, 'Expected revert, got ${error} instead');
		}

	});

	// deploy the contract for the second time
	it("Open contract and make sure paymentAmount is set to value sent", async () => {
		meta = await Escrow.new();
		contractAddress = meta.address;
		// Account 3 opens an escrow with .1 eth payment amount
		let res1 = await meta.createContract(beneficiary, {from: contractOpener, value: amount})
		let res2 = await meta.servers(contractOpener);
		assert(res2[2].eq(web3.utils.toBN(amount)), "Error in setting paymentAmount change");
		await meta.withdraw(beneficiary, {from: beneficiary});
	});

	it('Make sure paymentAmount cannot be changed and revert is thrown', async () => {

		let amount2 = web3.utils.toWei('0.2', 'ether');

		// Account 3 tries to open another escrow with .2 eth payment amount
		try {
			await meta.createContract(beneficiary, {from: contractOpener, value: amount2});
			assert(false, 'Expected revert but did not receive this');
			await meta.withdraw(beneficiary, {from: beneficiary});
		} catch (error) {
			const revertFound = error.message.search('revert') >= 0;
			assert(revertFound, 'Expected revert, got ${error} instead');
			let res2 = await meta.servers(contractOpener);
			assert(res2[2].eq(web3.utils.toBN(amount)), "Error in rejecting paymentAmount change");
			await meta.withdraw(beneficiary, {from: beneficiary});
		}
	});

	it('Make sure money can be deposited and returned after escrow period', async () => {
		let acc9StartBal = web3.utils.toBN(await web3.eth.getBalance(beneficiary));

		let depositer = accounts[3];
		let acc3StartBal = web3.utils.toBN(await web3.eth.getBalance(depositer));

		let usedGas = new Array(10).fill(new BN(0));

		let result = await meta.openNewEscrow(contractOpener, timeout, {from: depositer, value: depositAmount});
		usedGas[3] = usedGas[3].add(web3.utils.toBN(await gasHelper.getUsedGas(result)));

		timeHelper.skipTime(3);

		let res2 = await meta.submitRefund(contractOpener, depositer, {from: depositer});
		usedGas[3] = usedGas[3].add(web3.utils.toBN(await gasHelper.getUsedGas(res2)));

		timeHelper.skipTime(10);

		let res3 = await meta.processRefund(contractOpener, depositer, depositer, {from: depositer});
		usedGas[3] = usedGas[3].add(web3.utils.toBN(await gasHelper.getUsedGas(res3))); 

		let res4 = await meta.withdraw(depositer, {from: depositer});
		usedGas[3] = usedGas[3].add(web3.utils.toBN(await gasHelper.getUsedGas(res4)));

		let res5 = await meta.withdraw(beneficiary, {from: beneficiary});
		usedGas[9] = usedGas[9].add(web3.utils.toBN(await gasHelper.getUsedGas(res5)));

		let acc9EndBal = web3.utils.toBN(await web3.eth.getBalance(beneficiary));
		let acc3EndBal = web3.utils.toBN(await web3.eth.getBalance(depositer));

		assert(acc9EndBal.eq(acc9StartBal.add(web3.utils.toBN(amount)).sub(usedGas[9])), "Error in the amount paid to beneficiary");
		assert(acc3EndBal.eq(acc3StartBal.sub(web3.utils.toBN(amount)).sub(usedGas[3])), "Error in returning funds");
	});

	it('Make sure an escrow cant be opened with the same provider after its been closed', async () => {
		try {
			await meta.openNewEscrow(contractOpener, timeout, {from: accounts[3], value: amount});
			assert(false, 'Expected revert but did not receive this');
		} catch (error) {
			const revertFound = error.message.search('revert') >= 0;
			assert(revertFound, 'Expected revert but got $(error) instead');
		};
	});

	it('Make sure money can be deposited and claimed by two accounts', async () => {
		let acc9StartBal = web3.utils.toBN(await web3.eth.getBalance(beneficiary));

		let depositer = accounts[4];
		let acc4StartBal = web3.utils.toBN(await web3.eth.getBalance(depositer));

		let claimer = accounts[5];
		let acc5StartBal = web3.utils.toBN(await web3.eth.getBalance(claimer));

		let usedGas = new Array(10).fill(new BN(0));

		let result = await meta.openNewEscrow(contractOpener, timeout, {from: depositer, value: depositAmount});
		usedGas[4] = usedGas[4].add(web3.utils.toBN(await gasHelper.getUsedGas(result)));
		
		timeHelper.skipTime(3);

		let res2a = await meta.submitRefund(contractOpener, depositer, {from: depositer});
		usedGas[4] = usedGas[4].add(web3.utils.toBN(await gasHelper.getUsedGas(res2a)));

		let res2b = await meta.submitRefund(contractOpener, claimer, {from: depositer});
		usedGas[4] = usedGas[4].add(web3.utils.toBN(await gasHelper.getUsedGas(res2b)));

		timeHelper.skipTime(10);

		let res3a = await meta.processRefund(contractOpener, depositer, depositer, {from: depositer});
		usedGas[4] = usedGas[4].add(web3.utils.toBN(await gasHelper.getUsedGas(res3a)));

		let res3b = await meta.processRefund(contractOpener, depositer, claimer, {from: depositer});
		usedGas[4] = usedGas[4].add(web3.utils.toBN(await gasHelper.getUsedGas(res3b)));

		let res4a = await meta.withdraw(depositer, {from:depositer});
		usedGas[4] = usedGas[4].add(web3.utils.toBN(await gasHelper.getUsedGas(res4a)));

		let res4b = await meta.withdraw(claimer, {from: claimer});
		usedGas[5] = usedGas[5].add(web3.utils.toBN(await gasHelper.getUsedGas(res4b)));

		let res4c = await meta.withdraw(beneficiary, {from: beneficiary});
		usedGas[9] = usedGas[9].add(web3.utils.toBN(await gasHelper.getUsedGas(res4c)));

		let returnedAmount = web3.utils.toBN((Number(depositAmount) - (Number(amount) * 2)) / 2);
		let amountPaid = web3.utils.toBN(Number(depositAmount) - (Number(returnedAmount) * 2));

		let acc9EndBal = web3.utils.toBN(await web3.eth.getBalance(beneficiary));
		let acc4EndBal = web3.utils.toBN(await web3.eth.getBalance(depositer));
		let acc5EndBal = web3.utils.toBN(await web3.eth.getBalance(claimer));

		assert(acc9EndBal.eq(acc9StartBal.add(amountPaid).sub(usedGas[9])), 'Error in the amount paid to beneficiary');
		assert(acc4EndBal.eq(acc4StartBal.sub(web3.utils.toBN(depositAmount)).add(returnedAmount).sub(usedGas[4])), 'Error in returning funds to depositer');
		assert(acc5EndBal.eq(acc5StartBal.add(returnedAmount).sub(usedGas[5])), 'Error in releasing funds to claimant');
	});

	it('Make sure eth can be deposited and claimed by two accounts, once with a signed refund', async () => {
		let acc9StartBal = web3.utils.toBN(await web3.eth.getBalance(beneficiary));

		let depositer = accounts[6];
		let acc6StartBal = web3.utils.toBN(await web3.eth.getBalance(depositer));

		let claimer = accounts[7];
		let acc7StartBal = web3.utils.toBN(await web3.eth.getBalance(claimer));

		let usedGas = new Array(10).fill(new BN(0));

		let result = await meta.openNewEscrow(contractOpener, timeout, {from: depositer, value: depositAmount});
		usedGas[6] = usedGas[6].add(web3.utils.toBN(await gasHelper.getUsedGas(result)));

		timeHelper.skipTime(3);

		let res2a = await meta.submitRefund(contractOpener, depositer, {from: depositer});
		usedGas[6] = usedGas[6].add(web3.utils.toBN(await gasHelper.getUsedGas(res2a)));

		let hash = web3.utils.soliditySha3(meta.address, contractOpener, depositer, claimer);
		let signedData = await web3.eth.sign(hash, depositer);
		let sig = signedData.slice(2);
		let r = `0x${sig.slice(0,64)}`;
		let s = `0x${sig.slice(64,128)}`;
		let v = Number(sig.slice(128,130)) + 27;

		let res2b = await meta.submitSignedRefund(hash, v, r, s, contractOpener, depositer, claimer, {from: claimer});
		usedGas[7] = usedGas[7].add(web3.utils.toBN(await gasHelper.getUsedGas(res2b)));

		timeHelper.skipTime(10);

		let res3a = await meta.processRefund(contractOpener, depositer, depositer, {from: depositer});
		usedGas[6] = usedGas[6].add(web3.utils.toBN(await gasHelper.getUsedGas(res3a)));

		let res3b = await meta.processRefund(contractOpener, depositer, claimer, {from: depositer});
		usedGas[6] = usedGas[6].add(web3.utils.toBN(await gasHelper.getUsedGas(res3b)));

		let res4a = await meta.withdraw(depositer, {from: depositer});
		usedGas[6] = usedGas[6].add(web3.utils.toBN(await gasHelper.getUsedGas(res4a)));

		let res4b = await meta.withdraw(claimer, {from: claimer});
		usedGas[7] = usedGas[7].add(web3.utils.toBN(await gasHelper.getUsedGas(res4b)));

		let res4c = await meta.withdraw(beneficiary, {from: beneficiary});
		usedGas[9] = usedGas[9].add(web3.utils.toBN(await gasHelper.getUsedGas(res4c)));

		let returnedAmount = web3.utils.toBN((Number(depositAmount) - (Number(amount) * 2)) / 2);
		let amountPaid = web3.utils.toBN(Number(depositAmount) - (Number(returnedAmount) * 2));

		let acc9EndBal = web3.utils.toBN(await web3.eth.getBalance(beneficiary));
		let acc6EndBal = web3.utils.toBN(await web3.eth.getBalance(depositer));
		let acc7EndBal = web3.utils.toBN(await web3.eth.getBalance(claimer));

		assert(acc9EndBal.eq(acc9StartBal.add(amountPaid).sub(usedGas[9])), 'Error in the amount paid to beneficiary');
		assert(acc6EndBal.eq(acc6StartBal.sub(web3.utils.toBN(depositAmount)).add(returnedAmount).sub(usedGas[6])), 'Error in returning funds to depositer');
		assert(acc7EndBal.eq(acc7StartBal.add(returnedAmount).sub(usedGas[7])), 'Error releasing funds to claimant');
	});

	it('Make sure the delay can be hastened', async () => {
		let depositer = accounts[8];
		let origTimeout = 100;

		let result = await meta.openNewEscrow(contractOpener, origTimeout, {from: depositer, value: depositAmount});
		
		let res2 = await meta.users(contractOpener, depositer);
		let initialTimeout = res2[0]; 
		let hastenExpiryBy = 50;

		let hash = web3.utils.soliditySha3(meta.address, contractOpener, depositer, hastenExpiryBy);
		let signedData = await web3.eth.sign(hash, contractOpener);
		let sig = signedData.slice(2);
		let r = `0x${sig.slice(0, 64)}`;
		let s = `0x${sig.slice(64, 128)}`;
		let v = Number(sig.slice(128, 130)) + 27;

		let res3 = await meta.hastenExpiry(hash, v, r, s, contractOpener, hastenExpiryBy, {from: depositer});

		let res4 = await meta.users(contractOpener, depositer);
		let newTimeout = res4[0];

		assert.equal(initialTimeout.toNumber(), Number(newTimeout)+hastenExpiryBy, 'New timout not equal to old timeout plus delay');
	});

			
});
